// console.log("TGIF");

// What are conditional statements?

// Conditional statements allow us to control the flow of our program
// It allows us to run statement / instruction if a condition is met or run another separate instruction if otherwise.

// [section] if, else if and else statement

let numA = -1;

/*
	if statement
		- it will execute the statement if a specified condition is met/true .
*/

if (numA<0){
	console.log("Hello");
}

console.log(numA<0);

/*
	Syntax:
		if(condition){
			statemen;
		}
*/
// The result of the expression added in the if's condition must result to true, else, the statement inside if() will not run.

// lets update the variable and run an if statement with the same condition:


numA =0;
if (numA<0){
	console.log("Hello again if numA is 0!");
}
console.log(numA<0);
// It will not run because the expression now results to false

let city = "New York";
if (city === "New York") {
	console.log("Welcome to New York!");	
}

// else if clause
/*
	-execute a statement if previous conditions are false and if the specefied condition is true
	-The "else if" clause is optional an can be added to capture additional conditions  to change the flow of a program.
*/

let numH = 1;
if (numH < 0) {
	console.log("Hello from numH!");
}
else if (numH > 0) {
	console.log("Hi I'm H!");
}
// We were able to run the else if() statement after we evaluated that  the if condition was failed/false.
// If the if() condition was passed and run, we will no longer evaluate to else if() and end the process there.

if (numH > 0) {
	console.log("Hello from numH!");
}
else if(numH === 1){
	console.log("Hi I'm the second condition met!");
}
else if (numH < 0) {
	console.log("Hi I'm H!");
}
console.log(numH > 0);
console.log(numH === 1);
console.log(numH < 0);



// else if() statement was not executed bcoz the if statement was able to run, the evaluation of the whole statement stops there.

city = "Tokyo";
if (city === "New York") {
	console.log("Welcome to New York!");
}
else if(city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan")
}

// else statement
/*
	-Executes a statement if all other conditions are false / not ment.
	-Else statement is optional and can be added to capture any other results to change the flow of a program.
*/

numH = 2;
if(numH<0){
	console.log("Hello I'm numH");
}
else if(numH > 2){
	console.log("numH is greater than 2");
}
else if (numH > 3) {
	console.log("numH is greater than 3");
}
else{
	console.log("numH from else");
}
/*
	since all of the preceding if and else if conditions failed, the else statement was run instead.

	Else and else if statement should only be added if there is a preceeding if condition, else, statements by itself will not work, however, if statemets will work even if there is no else statement.
*/

// if, else if and else statement with functions
/*
	-Most of the times we would like to use if, else if,and else statements with dfunctions to control the flow  of our application.
	-By including them insie the functions, we can decide when certian condition will be checkedinstead of executing statements when the JavaScript loads.
	-The "return" statement can utilized with conditional statements in combination with functions to change values to be used for to other features of our application.
*/

let message;
function determinTyphoonIntensity(windSpeed) {
	if(windSpeed<0){
		return "Invalid argument";
	}
	else if(windSpeed>0 && windSpeed < 30){
		return "Not a typhoon yet."
	}
	else if (windSpeed <= 60) {
		return "Tropical depression detected."
	}
	else if (windSpeed >=61 && windSpeed <= 88) {
		return "Tropical Storm detected."
	}
	else if (windSpeed>=89 && windSpeed <=117) {
		return "Severe Tropical Storm detected.";
	}
	else{
		return "Typhoon detected.";
	}
}
	
	// Return the strings to the variable message thet invoked.
	message = determinTyphoonIntensity(-123);
	console.log(message);

	/*
		-We can further control the flow of our program based on conditions a nd changing variables and results.
		-Due to the conditional statements created in the situation,  we were able to reassign it's value and us it's new value to print different output.

		-console.warn() is good way to print wrnings in our console that could help us developers act on certain output within our code.
	*/

if (message === "Typhoon detected.") {
	console.warn(message)
}

// [Section] Truthy and Falsy
//  Truthy always true unless ereassign ang Boolean value.
/*Falsy 2. 0
        3. -0
        4. ""
        5. null
        6. undefined
        7. NaN*/

// Truthy Examples
if (true) {
	console.log("Truthy");
}

if (1) {
	console.log("Truthy");
}
if ([]) {
	console.log("Truthy");
}

// Falsy examples
if (false) {
	console.log("Falsy");
}
if (0) {
	console.log("Falsy");
}
if (undefined) {
	console.log("Falsy");
}

// [section] Conditional (Ternary) Operator

/*
	The conditional (ternary) Operator
	1. condition
	2. expression to execute if the condition is truthy
	3. expression if the condition is falsy

	-can be used as an alternative to an "if alse" statement.
	-Ternary operators have an implecit "return" statemen meaning without return keyword, the resulting expressions can be stored in a variable 
	-commonly used for single statement execution where result consist of only one line of code

	syntax:
	(expression)? ifTrue: ifFalse;

*/
	
	// single statement execution
	/*let ternaryResult = (1<18) ? true : false;*/
	let ternaryResult = (1<18) ? 1 : 2;
	console.log	("Result of Ternary Operator:" + ternaryResult);

	//Multiple Statement execution
	// Both functions perform two separate tasks which changes the value of  the
	// "name" variable are returns the result storing it in the "legalAge" variable

	let name;

	function isOfLegalAge(){
		name = 'John';
		return "You are of the legal age!";
	}

	function isUnderAge() {
		name = 'Jane';
		return "You are under age limit";
	}

	// The parseInt() function converts the input recieve into a number data type
	let	age = parseInt(prompt("What is your age?"));
	console.log(age);

	let legalAge = (age >=18)? isOfLegalAge():isUnderAge();
	console.log("Result of Ternary Operatorin Functions:" + legalAge+ ", "+name);

// [Section] Switch statement
	/*
		The Switch statement evaluates an expressions and matches the expression's value to a case claus.
	*/

	/*
		Syntax:
		switch (expression){
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}

	*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch(day){
	case 'monday':
		console.log("The color of the day is red!");
		break;
	case 'tuesday':
		console.log("The color of the day is orange!");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow!");
		break;
	case 'thursday':
		console.log("The color of the day is green!");
		break;
	case 'friday':
		console.log("The color of the day is blue!");
		break;
	case 'saturday':
		console.log("The color of the day is indigo!");
		break;
	case 'sunday':
		console.log("The color of the day is viole!");
		break;
	default:
		console.log("Please input a valid day.");
		break;
}

// [section] Try-catch-finally statement

	// "try catch" statement are commonly use for error handling.
	// There are instances when the application returns an error/warning that is not necessarily an error in the context of our code.
	// Thes errors are result of an attempt of the programming language to help developers in creating effecient code.
	// They are used to specify a response whenever an exception/error is recieved.

function showIntensityAlert(windSpeed){
	try{
		alerat(determinTyphoonIntensity(windSpeed))
	}
	catch(error){
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show alert.");
	}
}

showIntensityAlert(110);